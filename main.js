function filterBy(arr, type) {

    const array = [];
    arr.forEach(num => {
        if (typeof num !== type) {
            array.push(num);
        }
    });
    return array;
}

console.log(filterBy([213, 123, 0, 'lola', 35453, 11232, 'Hello World', 999, 2349834, 'danit'], 'number'));